export class Item {
    Id: number;
    Name: string;
    Description: string;
    Price: number;
    Img: string;
    Amount: number;
    Count: number = 1;
}