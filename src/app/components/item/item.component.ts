import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ItemService } from '../../services/item.service';
import { Item } from '../../models/item';

const maxLineLength = 26;

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input()
  Item: Item;

  @Input()
  isCart: boolean;

  @ViewChild('amount') amountInput;

  @Output()
  public onZeroAmount = new EventEmitter<any>();

  @Output()
  public onCountChange = new EventEmitter<number>();

  Description: string[] = [];
  incrementDisabled: boolean;
  decrementDisabled: boolean;

  constructor(
    private _itemService: ItemService
  ) {
    this.isCart = false;
    this.decrementDisabled = false;
  }

  ngOnInit() {
    let rawDescription = this.Item.Description;
    let words = rawDescription.split(' ');
    let lineLength = 0;
    let i = 0;
    this.Description.push('');
    words.forEach(word => {
      lineLength += word.length + 1;
      if (lineLength < maxLineLength)
        this.Description[i] += word + ' ';
      else {
        if (i === 1) {
          this.Description[i] += '…';
        }
        i++;
        lineLength = word.length + 1;
        this.Description.push(word + ' ');
      }
    });

    this.decrementDisabled = !this.isCart;
  }

  public increment() {
    this.Item.Count++;
    this.onCountChange.emit(this.Item.Price);
    this.decrementDisabled = false;
    if (this.Item.Count === this.Item.Amount) this.incrementDisabled = true;
  }

  public decrement() {
    this.Item.Count--;
    this.onCountChange.emit(-this.Item.Price);
    this.incrementDisabled = false;
    if (this.Item.Count === 1 && !this.isCart) this.decrementDisabled = true;
    if (this.Item.Count === 0) this.onZeroAmount.emit(this.Item);
  }

  public validateAmount(value: number) {
    if (value === null) return;

    if (value > 0 && value <= this.Item.Amount) {
      this.onCountChange.emit(this.Item.Price * (value - this.Item.Count));
      this.Item.Count = value;      
    }
    else if (value <= 0) {
      this.onCountChange.emit(this.Item.Price * (1 - this.Item.Count));
      this.Item.Count = 1;
      this.amountInput.nativeElement.value = this.Item.Count.toString();
    }
    else if (value > this.Item.Amount) {
      this.onCountChange.emit(this.Item.Price * (this.Item.Amount - this.Item.Count));
      this.Item.Count = this.Item.Amount;
      this.amountInput.nativeElement.value = this.Item.Count.toString();
    }
  }

}
