import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const items = [
      { Id: '1', Name: 'Burger', Description: 'Краткое описание товара со всеми его прелестями и особенностями.', Price: 200, Img: 'assets/img/item1.jpg', Amount: 4, Count: 1 },
      { Id: '2', Name: 'Pizza', Description: 'Краткое описание товара со всеми его прелестями и особенностями.', Price: 300, Img: 'assets/img/item2.jpg', Amount: 5, Count: 1 },
      { Id: '3', Name: 'Fried buns', Description: 'Краткое описание товара со всеми его прелестями и особенностями.', Price: 150, Img: 'assets/img/item3.jpg', Amount: 6, Count: 1 },
    ];
    return { items };
  }
}