import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Item } from '../models/item';
import { Observable } from 'rxjs/Observable';

const itemsUrl = 'api/items';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ItemService {
  

  constructor(
    private _http: HttpClient,
    ) { }

  getItems(): Observable<Item[]> {
    return this._http.get<Item[]>(itemsUrl);
  }

  getItem(id: number): Observable<Item> {
    return this._http.get<Item>(`${itemsUrl}/$id`);
  }
}
