import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { Item } from './models/item';
import { ItemService } from './services/item.service';
import { ItemComponent } from './components/item/item.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';

  ShoppingCart = 'assets/img/shopping-cart.png';

  TotalAmount: number = 0;

  @ViewChildren(ItemComponent) itemComponents: QueryList<ItemComponent>;

  Items: Item[];
  Cart: Item[] = [];

  constructor(
    private _itemService: ItemService
  ) { }

  ngOnInit() {
    this._itemService.getItems().subscribe(
      items => this.Items = items
    );
  }

  public onItemDrop(event: any) {
    let cartItem = this.Cart.find(x => x.Id === event.dragData.Id);

    if (!cartItem){
      this.Cart.unshift(this.getItem(event.dragData));
      this.TotalAmount += event.dragData.Count * event.dragData.Price;
    }
    else {
      let itemComponent = this.itemComponents.find(x => x.Item.Id === cartItem.Id && x.isCart);
      let oldCount = itemComponent.Item.Count;
      itemComponent.Item.Count += event.dragData.Count;
      itemComponent.validateAmount(itemComponent.Item.Count);
      this.TotalAmount += (itemComponent.Item.Count - oldCount) * itemComponent.Item.Price;
    }
  }

  public outsideItemDrop(event: any) {
    let cartItem = this.Cart.find(x => x.Id === event.dragData.Id);
    this.TotalAmount -= cartItem.Count * cartItem.Price;
    let i = this.Cart.indexOf(cartItem);
    this.Cart.splice(i, 1);
  }

  public removeItemFromCart(event: any) {
    let cartItem = this.Cart.find(x => x.Id === event.Id);
    let i = this.Cart.indexOf(cartItem);
    this.Cart.splice(i, 1);
  }

  public getItem(item: Item): Item {
    return { ...item };    
  }

  public updateTotalAmount(amount: number) {
    this.TotalAmount += amount;
  }
}
